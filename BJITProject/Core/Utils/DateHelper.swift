//
//  DateHelper.swift
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation


struct DateHelper {
    
    private static let calendar = Calendar.current
    private static let dateformatter:DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a(EEE)"
        return dateFormatter
    }()
    
    
    static func weekDaySymbol(date:Date) -> String {
        let weekNumber = calendar.component(.weekday, from: date)
        let weekSymbol = dateformatter.shortStandaloneWeekdaySymbols[weekNumber]
        return weekSymbol
    }
    
    static func timeInHourWeekDay(date:Date) -> String {
        return dateformatter.string(from: date)
    }
}
