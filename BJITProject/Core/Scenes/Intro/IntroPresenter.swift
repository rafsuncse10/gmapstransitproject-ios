//
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation

class IntroPresenter: BasePresenter<IntroView>, IntroActions {
    
    var routes = [Route]()
    
    override init() {
        super.init()
    }
    
    func onRouteClicked(at index: Int) {
        let selectedRoute = routes[index]
        view?.showNextScreen(for: selectedRoute)
    }
    
    func onLocationSubmitted(origin:String,destination: String) {
        view?.startLoading()
        
        GMapManager.fetchRoutes(origin: origin, destination: destination) {[weak self] (routes) in
            guard let routes = routes else {
                self?.view?.finishLoading()
                return
            }
            /*
            routes.enumerated().forEach({ (index,route) in
                let eachRoute = route.legs?.first?.steps?.compactMap({ (step) -> String? in
                    
                    let walkingSteps = step.steps?.compactMap({ (step) -> String? in
                        return step.travel_mode?.rawValue
                    }).joined(separator: "‣")
                    
                    return step.travel_mode?.rawValue.appending(walkingSteps ?? "")
                }).joined(separator: "☛☛")
                
                print("\(index) \(eachRoute ?? "")")
                
            })
            */
            self?.routes = routes
            self?.view?.finishLoading()
        }
    }
    
    private func addRoute(_ route: Route?) {
        guard let route = route else {
            return
        }
        
        routes.append(route)
        
        view?.updateRouteView()
    }
    
}
