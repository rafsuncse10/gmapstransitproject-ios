//
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation

protocol IntroView: BaseView {
    func updateRouteView()
    func showNextScreen(for route:Route)
}
