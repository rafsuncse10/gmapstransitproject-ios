//
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation

protocol IntroActions {
    func onLocationSubmitted(origin:String,destination: String)
    func onRouteClicked(at index:Int)
}
