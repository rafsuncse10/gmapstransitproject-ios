//
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation

class DetailPresenter: BasePresenter<DetailView>, DetailActions {
    
    var route:Route!
    
    override init() {
        super.init()
    }
    
    //protocol stubs
    func onDetailLocationRequested() {
        
    }
    
    //view impl
    
    var isWalkingModeOnly:Bool{
//        return route.legs?.first?.arrival_time != nil
        return (route.legs?.first?.steps?.count ?? 0) == 1
    }
    
    func getTableOriginHeader() -> (title:String?,desc:String?){
        
        let title = route.legs?.first?.start_address
        let description = route.legs?.first?.departure_time?.text
        return (title,description)
        
    }
    func getTableDestinationFooter() -> (title:String?,desc:String?) {
        
        let title = route.legs?.first?.end_address
        let description = route.legs?.first?.arrival_time?.text
        return (title,description)
        
    }
    
    func getFlatItems() -> [Steps]? {
        if isWalkingModeOnly{
            return route.legs?.first?.steps?.first?.steps
        }else{
            return route.legs?.first?.steps
        }
    }
    
    //Uncomment when collapse feature available for ride ,No Collapse RightNow
    /*
     
     func getTableHeader() -> String {
        guard let leg = route.legs?.first else { return "EMPTY HEADER" }
     
        return leg.start_address ?? ""
     }
     func getTableFooter() -> String {
        guard let leg = route.legs?.first else { return "EMPTY FOOTER" }
     
        return leg.end_address ?? ""
     }
     
    func getSections() -> [RouteSection]{
        guard let leg = route.legs?.first else { return [] }
        

        
        let routeSections:[RouteSection]? = leg.steps?.map({ (step) -> RouteSection in
            return RouteSection(step: step)
        })
        return routeSections ?? []
        
    }
    */
    
}
