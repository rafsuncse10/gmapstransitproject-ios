//
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation

protocol BaseView {
    func startLoading()
    func finishLoading()
}

extension BaseView {
    
    func startLoading() {
        
    }
    
    func finishLoading() {
        
    }
}
