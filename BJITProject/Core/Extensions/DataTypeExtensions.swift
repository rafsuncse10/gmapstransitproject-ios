//
//  DataTypeExtensions.swift
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation


extension Int{
    var toDouble:Double{
        return Double(self)
    }
}

extension Character{
    var toString:String{
        return String(self)
    }
}

extension String{
    var toAttributedHTMLString: NSAttributedString?{
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [.documentType: NSAttributedString.DocumentType.html,
                      .characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil) else { return nil }
        return html
    }
}



extension NSObjectProtocol {
    
    var className: String {
        return String(describing: Self.self)
    }
}
