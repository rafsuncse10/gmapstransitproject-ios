//
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation

struct GMapManager {
    
    
    static func fetchRoutes(origin:String,destination:String,completion:@escaping ([Route]?) -> Void) {
        
//        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?alternatives=true&origin=bieberstrasse,+dusseldorf&destination=norf,+neuss&sensor=false&mode=transit&departure_time=now&key=AIzaSyAGeBYBMLmumQgc7YvcaAxS2ZJF-nkYKOg")
        
        
        var urlComponents = URLComponents(string: "https://maps.googleapis.com")!
        urlComponents.path = "/maps/api/directions/json"
        urlComponents.queryItems = [
            URLQueryItem(name: "alternatives", value: "true"),
            URLQueryItem(name: "origin", value: origin),
            URLQueryItem(name: "destination", value: destination),
            URLQueryItem(name: "mode", value: "transit"),
//            URLQueryItem(name: "departure_time", value: "now"),
//            URLQueryItem(name: "region", value: "jp"),
            URLQueryItem(name: "key", value: GMapsConfig.DIRECTION_API_KEY)
        ]
        
        let task = URLSession.shared.dataTask(with: urlComponents.url!) { (data, response, error) in
            DispatchQueue.main.async {
                guard let data = data, let responseModel = try? JSONDecoder().decode(GMapRouteFetchDecoder.self, from: data) else {
                    return completion(nil)
                }
                
                completion(responseModel.routes)
            }
            
            
        }
        
        task.resume()
        
        
    }
    
    
}
