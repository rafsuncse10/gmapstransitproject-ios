/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
*/

import Foundation


struct Bounds : Codable {
	let northeast : Northeast?
	let southwest : Southwest?

	enum CodingKeys: String, CodingKey {

		case northeast = "northeast"
		case southwest = "southwest"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		northeast = try values.decodeIfPresent(Northeast.self, forKey: .northeast)
		southwest = try values.decodeIfPresent(Southwest.self, forKey: .southwest)
	}

}
