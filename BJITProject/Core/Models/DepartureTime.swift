/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
*/

import Foundation


struct DepartureTime : Codable {
	let text : String?
	let time_zone : String?
	let value : Int?

	enum CodingKeys: String, CodingKey {

		case text = "text"
		case time_zone = "time_zone"
		case value = "value"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		text = try values.decodeIfPresent(String.self, forKey: .text)
		time_zone = try values.decodeIfPresent(String.self, forKey: .time_zone)
		value = try values.decodeIfPresent(Int.self, forKey: .value)
	}

}
