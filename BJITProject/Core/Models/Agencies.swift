/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
*/

import Foundation


struct Agencies : Codable {
	let name : String?
	let url : String?

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case url = "url"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		url = try values.decodeIfPresent(String.self, forKey: .url)
	}

}
