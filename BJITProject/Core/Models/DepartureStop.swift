/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
*/

import Foundation


struct DepartureStop : Codable {
	let location : Location?
	let name : String?

	enum CodingKeys: String, CodingKey {

		case location = "location"
		case name = "name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		location = try values.decodeIfPresent(Location.self, forKey: .location)
		name = try values.decodeIfPresent(String.self, forKey: .name)
	}

}
