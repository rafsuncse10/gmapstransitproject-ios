/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
*/

import Foundation


struct Vehicle : Codable {
	let icon : String?
	let local_icon : String?
	let name : String?
	let type : String?

	enum CodingKeys: String, CodingKey {

		case icon = "icon"
		case local_icon = "local_icon"
		case name = "name"
		case type = "type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		icon = try values.decodeIfPresent(String.self, forKey: .icon)
		local_icon = try values.decodeIfPresent(String.self, forKey: .local_icon)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		type = try values.decodeIfPresent(String.self, forKey: .type)
	}

}
