/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
*/

import Foundation


struct GeocodedWaypoints : Codable {
	let geocoder_status : String?
	let partial_match : Bool?
	let place_id : String?
	let types : [String]?

	enum CodingKeys: String, CodingKey {

		case geocoder_status = "geocoder_status"
		case partial_match = "partial_match"
		case place_id = "place_id"
		case types = "types"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		geocoder_status = try values.decodeIfPresent(String.self, forKey: .geocoder_status)
		partial_match = try values.decodeIfPresent(Bool.self, forKey: .partial_match)
		place_id = try values.decodeIfPresent(String.self, forKey: .place_id)
		types = try values.decodeIfPresent([String].self, forKey: .types)
	}

}
