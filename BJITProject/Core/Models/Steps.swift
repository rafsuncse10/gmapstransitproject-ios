/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
*/

import Foundation

struct Steps : Codable {
	let distance : Distance?
	let duration : Duration?
	let end_location : EndLocation?
	let html_instructions : String?
    let maneuver:String?
	let polyline : Polyline?
	let start_location : StartLocation?
    let steps : [Steps]?//if walking
	let transit_details : TransitDetails?//if station
	let travel_mode : TravelMode?

	enum CodingKeys: String, CodingKey {

		case distance = "distance"
		case duration = "duration"
		case end_location = "end_location"
        case html_instructions = "html_instructions"
		case maneuver = "maneuver"
		case polyline = "polyline"
		case start_location = "start_location"
        case steps = "steps"
		case transit_details = "transit_details"
		case travel_mode = "travel_mode"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		distance = try values.decodeIfPresent(Distance.self, forKey: .distance)
		duration = try values.decodeIfPresent(Duration.self, forKey: .duration)
		end_location = try values.decodeIfPresent(EndLocation.self, forKey: .end_location)
        html_instructions = try values.decodeIfPresent(String.self, forKey: .html_instructions)
		maneuver = try values.decodeIfPresent(String.self, forKey: .maneuver)
		polyline = try values.decodeIfPresent(Polyline.self, forKey: .polyline)
		start_location = try values.decodeIfPresent(StartLocation.self, forKey: .start_location)
        steps = try values.decodeIfPresent([Steps].self, forKey: .steps)
		transit_details = try values.decodeIfPresent(TransitDetails.self, forKey: .transit_details)
		travel_mode = try values.decodeIfPresent(TravelMode.self, forKey: .travel_mode)
	}

}
