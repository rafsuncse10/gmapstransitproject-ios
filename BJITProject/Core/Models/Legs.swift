/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
*/

import Foundation


struct Legs : Codable {
	let arrival_time : ArrivalTime?
	let departure_time : DepartureTime?
	let distance : Distance?
	let duration : Duration?
	let end_address : String?
	let end_location : EndLocation?
	let start_address : String?
	let start_location : StartLocation?
	let steps : [Steps]?
	let traffic_speed_entry : [String]?
	let via_waypoint : [String]?

	enum CodingKeys: String, CodingKey {

		case arrival_time = "arrival_time"
		case departure_time = "departure_time"
		case distance = "distance"
		case duration = "duration"
		case end_address = "end_address"
		case end_location = "end_location"
		case start_address = "start_address"
		case start_location = "start_location"
		case steps = "steps"
		case traffic_speed_entry = "traffic_speed_entry"
		case via_waypoint = "via_waypoint"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		arrival_time = try values.decodeIfPresent(ArrivalTime.self, forKey: .arrival_time)
		departure_time = try values.decodeIfPresent(DepartureTime.self, forKey: .departure_time)
		distance = try values.decodeIfPresent(Distance.self, forKey: .distance)
		duration = try values.decodeIfPresent(Duration.self, forKey: .duration)
		end_address = try values.decodeIfPresent(String.self, forKey: .end_address)
		end_location = try values.decodeIfPresent(EndLocation.self, forKey: .end_location)
		start_address = try values.decodeIfPresent(String.self, forKey: .start_address)
		start_location = try values.decodeIfPresent(StartLocation.self, forKey: .start_location)
		steps = try values.decodeIfPresent([Steps].self, forKey: .steps)
		traffic_speed_entry = try values.decodeIfPresent([String].self, forKey: .traffic_speed_entry)
		via_waypoint = try values.decodeIfPresent([String].self, forKey: .via_waypoint)
	}

}
