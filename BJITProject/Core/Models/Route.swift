/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
*/

import Foundation


struct Route : Codable {
	let bounds : Bounds?
	let copyrights : String?
	let legs : [Legs]?
	let overview_polyline : OverviewPolyline?
	let summary : String?
	let warnings : [String]?
	let waypoint_order : [String]?

	enum CodingKeys: String, CodingKey {

		case bounds = "bounds"
		case copyrights = "copyrights"
		case legs = "legs"
		case overview_polyline = "overview_polyline"
		case summary = "summary"
		case warnings = "warnings"
		case waypoint_order = "waypoint_order"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		bounds = try values.decodeIfPresent(Bounds.self, forKey: .bounds)
		copyrights = try values.decodeIfPresent(String.self, forKey: .copyrights)
		legs = try values.decodeIfPresent([Legs].self, forKey: .legs)
		overview_polyline = try values.decodeIfPresent(OverviewPolyline.self, forKey: .overview_polyline)
		summary = try values.decodeIfPresent(String.self, forKey: .summary)
		warnings = try values.decodeIfPresent([String].self, forKey: .warnings)
		waypoint_order = try values.decodeIfPresent([String].self, forKey: .waypoint_order)
	}

}
