/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
*/

import Foundation


struct GMapRouteFetchDecoder : Codable {
	let geocoded_waypoints : [GeocodedWaypoints]?
	let routes : [Route]?
	let status : String?

	enum CodingKeys: String, CodingKey {

		case geocoded_waypoints = "geocoded_waypoints"
		case routes = "routes"
		case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		geocoded_waypoints = try values.decodeIfPresent([GeocodedWaypoints].self, forKey: .geocoded_waypoints)
		routes = try values.decodeIfPresent([Route].self, forKey: .routes)
		status = try values.decodeIfPresent(String.self, forKey: .status)
	}

}
