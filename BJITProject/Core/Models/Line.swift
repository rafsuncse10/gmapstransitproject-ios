/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
 */

import Foundation


struct Line : Codable {
	let agencies : [Agencies]?
	let short_name : String?
	let vehicle : Vehicle?
    let color:String?
    let text_color:String?
    
	enum CodingKeys: String, CodingKey {

		case agencies = "agencies"
		case short_name = "short_name"
        case vehicle = "vehicle"
        case color = "color"
		case text_color = "text_color"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		agencies = try values.decodeIfPresent([Agencies].self, forKey: .agencies)
		short_name = try values.decodeIfPresent(String.self, forKey: .short_name)
        vehicle = try values.decodeIfPresent(Vehicle.self, forKey: .vehicle)
        color = try values.decodeIfPresent(String.self, forKey: .color)
		text_color = try values.decodeIfPresent(String.self, forKey: .text_color)
        
	}

}
