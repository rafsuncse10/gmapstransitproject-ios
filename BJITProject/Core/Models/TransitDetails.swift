/*
 Swift Models Generated from JSON powered by http://www.json4swift.com
 Created by RAFAT TOUQIR RAFSUN on 29/10/18.
 Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
*/

import Foundation


struct TransitDetails : Codable {
	let arrival_stop : ArrivalStop?
	let arrival_time : ArrivalTime?
	let departure_stop : DepartureStop?
	let departure_time : DepartureTime?
	let headsign : String?
	let line : Line?
	let num_stops : Int?

	enum CodingKeys: String, CodingKey {

		case arrival_stop = "arrival_stop"
		case arrival_time = "arrival_time"
		case departure_stop = "departure_stop"
		case departure_time = "departure_time"
		case headsign = "headsign"
		case line = "line"
		case num_stops = "num_stops"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		arrival_stop = try values.decodeIfPresent(ArrivalStop.self, forKey: .arrival_stop)
		arrival_time = try values.decodeIfPresent(ArrivalTime.self, forKey: .arrival_time)
		departure_stop = try values.decodeIfPresent(DepartureStop.self, forKey: .departure_stop)
		departure_time = try values.decodeIfPresent(DepartureTime.self, forKey: .departure_time)
		headsign = try values.decodeIfPresent(String.self, forKey: .headsign)
		line = try values.decodeIfPresent(Line.self, forKey: .line)
		num_stops = try values.decodeIfPresent(Int.self, forKey: .num_stops)
	}

}
