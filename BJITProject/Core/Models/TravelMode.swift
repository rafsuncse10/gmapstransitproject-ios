//
//  TravelMode.swift
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation

enum TravelMode:String,Codable{
    case driving = "DRIVING"
    case walking = "WALKING"
    case bicycling = "BICYCLING"
    case transit = "TRANSIT"
}
