//
//  ImageDownloader.swift
//  BJITProject
//
//  Created by RAFAT TOUQIR RAFSUN on 2/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation

#if canImport(Kingfisher)
import Kingfisher
fileprivate let hasKingfisherFramework = true
#else
// provide alternative solution that does not depend on that module
fileprivate let hasKingfisherFramework = true
#endif

class ImageDownloader {
    
    init() {
        
    }
    
    func download(imageURL url:URL,completionHandler: @escaping (Data?, Error?) -> Void){
        if hasKingfisherFramework{
            KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil) { (image, error, type, url) in
                completionHandler(image?.pngData(),error)
            }
            
        }else{
            
            let dataTask = URLSession.shared.dataTask(with: url) {(rawData, response, rawError) in
                guard let data = rawData, rawError == nil else {
                    print(rawError?.localizedDescription ?? "")
                    completionHandler(nil,rawError)
                    return
                }
                
                completionHandler(data,nil)
            }
            
            dataTask.resume()
            
        }
        
        
        
        
    }
    
}
