//
//  AsyncTextAttachment.swift
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import UIKit
import MobileCoreServices

public protocol AsyncTextAttachmentDelegate:class
{
    /// Called when the image has been loaded
    func textAttachmentDidLoadImage(_ textAttachment: AsyncTextAttachment, displaySizeChanged: Bool)
}

public typealias AsyncTextAttachmentCompletion = (AsyncTextAttachment,Bool) -> Void

/// An image text attachment that gets loaded from a remote URL
public class AsyncTextAttachment: NSTextAttachment
{
    /// Remote URL for the image
    public var imageURL: URL?
    public var tag: Int?
    
    /// To specify an absolute display bounds.
    public var displayBounds: CGRect?
    
    /// if determining the display size automatically this can be used to specify a maximum width. If it is not set then the text container's width will be used
    public var maximumDisplayWidth: CGFloat?
    
    /// A delegate to be informed of the finished download
    public var onCompletion: AsyncTextAttachmentCompletion?
    
    /// Remember the text container from delegate message, the current one gets updated after the download
    weak var textContainer: NSTextContainer?
    
    /// The download task to keep track of whether we are already downloading the image
    private var downloadTask: ImageDownloader?
    
    /// The size of the downloaded image. Used if we need to determine display size
    private var originalImageSize: CGSize?
    
    /// Designated initializer
    public init(imageURL: URL? = nil, onCompletion: AsyncTextAttachmentCompletion? = nil)
    {
        self.imageURL = imageURL
        self.onCompletion = onCompletion
        
        super.init(data: nil, ofType: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public var image: UIImage? {
        didSet {
            originalImageSize = image?.size
        }
    }
    
    // MARK: - Helpers
    
    private func startAsyncImageDownload()
    {
        guard let imageURL = imageURL, contents == nil && downloadTask == nil else
        {
            return
        }
        
        downloadTask = ImageDownloader()
        downloadTask?.download(imageURL: imageURL, completionHandler: {[weak self] (data, error) in
            guard let `self` = self else { return }
            defer
            {
                // done with the task"
                self.downloadTask = nil
            }
            
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "")
                return
            }
            
            var displaySizeChanged = false
            
            self.contents = data
            
            let ext = imageURL.pathExtension as CFString
            if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, ext, nil)
            {
                self.fileType = uti.takeRetainedValue() as String
            }
            
            if let image = UIImage(data: data)
            {
                let imageSize = image.size
                
                if self.displayBounds == nil
                {
                    displaySizeChanged = true
                }
                
                self.originalImageSize = imageSize
            }
            
            DispatchQueue.main.async {[weak self] in
                guard let `self` = self else { return }
                // tell layout manager so that it should refresh
                if displaySizeChanged
                {
                    self.textContainer?.layoutManager?.setNeedsLayout(forAttachment: self)
                }
                else
                {
                    self.textContainer?.layoutManager?.setNeedsDisplay(forAttachment: self)
                }
                
                // notify the optional delegate
                self.onCompletion?(self,displaySizeChanged)
            }
        })
      
        
    }
    
    
    public override func image(forBounds imageBounds: CGRect, textContainer: NSTextContainer?, characterIndex charIndex: Int) -> UIImage? {
        if let image = image { return image }
        
        guard let contents = contents,let image = UIImage(data: contents) else
        {
            // remember reference so that we can update it later
            self.textContainer = textContainer
            
            startAsyncImageDownload()
            
            return nil
        }
        
        return image
    }
    
    public override func attachmentBounds(for textContainer: NSTextContainer?, proposedLineFragment lineFrag: CGRect, glyphPosition position: CGPoint, characterIndex charIndex: Int) -> CGRect {
        if let displayBounds = displayBounds
        {
            return displayBounds
        }
        
        if let imageSize = originalImageSize
        {
            let maxWidth = maximumDisplayWidth ?? lineFrag.size.width
            let factor = maxWidth / imageSize.width
            
            return CGRect(origin: displayBounds?.origin ?? .zero, size:CGSize(width: Int(imageSize.width * factor), height: Int(imageSize.height * factor)))
        }
        
        return .zero
    }
}
