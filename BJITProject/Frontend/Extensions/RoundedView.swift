//
//  RoundedView.swift
//  Created by RAFAT TOUQIR RAFSUN on 4/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import UIKit

class RoundedView:UIView{
    let maskLayer = CAShapeLayer()
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        let haldWidth = self.bounds.width/2
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.allCorners], cornerRadii: CGSize(width: haldWidth, height: haldWidth))
        
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
        
    }
}
