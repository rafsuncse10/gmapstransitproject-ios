//
//  TransitMixedTransitTableViewCell.swift
//  BJITProject
//
//  Created by RAFAT TOUQIR RAFSUN on 4/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import UIKit

class TransitMixedTransitTableViewCell: UITableViewCell,ReusableView,NibLoadableView {

    
    @IBOutlet weak var imageViewTransit: UIImageView!
    
    @IBOutlet weak var labelFromLocation: UILabel!
    
    @IBOutlet weak var labelVehicleName_HeadSign: UILabel!
    
    @IBOutlet weak var labelVehicleDepartureTime: UILabel!
    
    @IBOutlet weak var labelVehicleDescription: UILabel!
    
    @IBOutlet weak var labelTotalStop: UILabel!
    
    
    @IBOutlet weak var labelToLocation: UILabel!
    

    @IBOutlet weak var stripViewForColoring: RoundedView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
