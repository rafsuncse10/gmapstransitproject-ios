//
//  WalkingOnlyODTableViewCell.swift
//  BJITProject
//
//  Created by RAFAT TOUQIR RAFSUN on 4/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import UIKit

class WalkingOnlyODTableViewCell: UITableViewCell,ReusableView,NibLoadableView {

    @IBOutlet weak var imageViewWalking: UIImageView!
    
    @IBOutlet weak var labelLocationTitile: UILabel!
    
    @IBOutlet weak var labelLocationDetail: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }
    
}
