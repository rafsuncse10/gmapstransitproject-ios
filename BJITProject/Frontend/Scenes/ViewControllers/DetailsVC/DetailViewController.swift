//
//  DetailViewController.swift
//  BJITProject
//
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    //presenter
    var detailPresenter:DetailPresenter!

    
    @IBOutlet weak var labelSteps: UILabel!
    
    @IBOutlet weak var labelDuration: UILabel!
    
    @IBOutlet weak var labelStartToEndTime: UILabel!
    
    @IBOutlet weak var labelDetails: UILabel!
    

    @IBOutlet weak var tableViewDetail: UITableView!
    
    
//    var sections:[RouteSection]!
    var items:[Steps]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Route Details"
        addCancelButton()
        getOverView()
        // Auto resizing the height of the cell
        tableViewDetail.estimatedRowHeight = 44.0
        tableViewDetail.rowHeight = UITableView.automaticDimension
        tableViewDetail.separatorColor = .clear
//        self.sections = detailPresenter.getSections()
        self.items = detailPresenter.getFlatItems()
        self.registerTableCells()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        detailPresenter.attach(this: self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        detailPresenter.detach(this: self)
    }

    
    
    func addCancelButton() {
        let backButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissVC))
        self.navigationItem.leftBarButtonItem = backButton
    }
    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func getOverView() {
        
        if detailPresenter.isWalkingModeOnly{
            let title = detailPresenter.route.legs?.first?.distance?.text ?? ""
            let duration = detailPresenter.route.legs?.first?.duration?.value?.toDouble.toStringInTime(style: .short) ?? ""
            let details = detailPresenter.route.warnings?.joined(separator: "\n") ?? ""
            
            self.labelSteps.text = title
            self.labelDetails.text = details
            self.labelDuration.text = duration
            
            self.labelStartToEndTime.text = "" // not using
            
        }else{
            
            let routeStepMSG = NSMutableAttributedString(string: "")
            
            detailPresenter.route.legs?.first?.steps?.enumerated().forEach{ index,step in
                
                if case .transit? = step.travel_mode{
                    
                    let transitIconUrlStr = step.transit_details?.line?.vehicle?.local_icon ?? step.transit_details?.line?.vehicle?.icon
                    if let transitIconStr = transitIconUrlStr,let transitIconUrl = URL(string: "https:" + transitIconStr){
                        //print(transitIconUrl.absoluteString)
                        let transitIconAttachment = AsyncTextAttachment(imageURL: transitIconUrl, onCompletion: {[weak self] (imageAttachment, isSizeChanged) in
                            self?.labelSteps.setNeedsDisplay()
                        })
                        transitIconAttachment.displayBounds = CGRect(x: 0, y: -4, width: 20, height: 20)
                        let transitIconAttachmentStr = NSAttributedString(attachment: transitIconAttachment)
                        
                        routeStepMSG.append(transitIconAttachmentStr)
                    }
                    
                    if let transitSName = step.transit_details?.line?.short_name{
                        let attributedTransitSName = NSMutableAttributedString(string: transitSName)
                        if let textColorHex = step.transit_details?.line?.text_color{
                            attributedTransitSName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hex: textColorHex), range: NSRange(location: 0, length: transitSName.count))
                        }
                        if let backGroundColorHex = step.transit_details?.line?.color{
                            attributedTransitSName.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor(hex: backGroundColorHex), range: NSRange(location: 0, length: transitSName.count))
                        }
                        
                        routeStepMSG.append(attributedTransitSName)
                    }
                    
                }else if case .walking? = step.travel_mode{
                    
                    
                    let walkingImageAttachment = AsyncTextAttachment()
                    walkingImageAttachment.image = #imageLiteral(resourceName: "Walking")
                    walkingImageAttachment.displayBounds = CGRect(x: 0, y: -4, width: 20, height: 20)
                    let walkingImageAttachmentStr = NSAttributedString(attachment: walkingImageAttachment)
                    
                    routeStepMSG.append(walkingImageAttachmentStr)
                    
                    if let stepDuration = step.duration?.value?.toDouble.toStringInTime(style: .abbreviated){
                        
                        let stepDurationAttr = NSAttributedString(string: stepDuration, attributes: [
                            NSAttributedString.Key.font : UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
                            NSAttributedString.Key.baselineOffset : -5
                            ])
                        
                        routeStepMSG.append(stepDurationAttr)
                        
                    }
                    
                }else{
                    let nonTransitTypeStr = step.travel_mode?.rawValue.first?.toString ?? ""
                    
                    routeStepMSG.append(NSAttributedString(string: nonTransitTypeStr))
                }
                
                let totalSteps = detailPresenter.route.legs!.first!.steps!.count
                if totalSteps > 1 && index != (totalSteps - 1){
                    routeStepMSG.append(NSAttributedString(string: " > "))
                }
            }
            let routeDuration = detailPresenter.route.legs?.first?.duration?.value?.toDouble.toStringInTime(style: .short)
            labelSteps.attributedText = routeStepMSG
            labelDuration.text = routeDuration
            
            
            labelStartToEndTime.text = "" //not using
            labelDetails.text = "" //not using
            
        }
    }

}



extension DetailViewController:DetailView{
    //stub
    func updateDetailView() {
        
    }
    
    
}
