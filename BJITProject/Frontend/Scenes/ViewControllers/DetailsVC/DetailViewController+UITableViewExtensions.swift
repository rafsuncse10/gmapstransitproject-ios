//
//  DetailViewController+UITableViewExtensions.swift
//  BJITProject
//
//  Created by RAFAT TOUQIR RAFSUN on 3/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import UIKit


extension DetailViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items?.count.advanced(by: numberOfHeaderFooter) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if detailPresenter.isWalkingModeOnly{
            return self.walkingModeOnlyCell(tableView, forIndexPath: indexPath)
        }else{
            return self.mixedTransitModeCell(tableView,forIndexPath:indexPath)
        }
    }
    
    
    func registerTableCells(){
        if detailPresenter.isWalkingModeOnly{
            self.tableViewDetail.register(WalkingOnlyODTableViewCell.self)
            self.tableViewDetail.register(WalkingOnlyStepTableViewCell.self)
        }else{
            self.tableViewDetail.register(TransitMixedODTableViewCell.self)
            self.tableViewDetail.register(TransitMixedWalkingTableViewCell.self)
            self.tableViewDetail.register(TransitMixedTransitTableViewCell.self)
        }
    }
    
    var numberOfHeaderFooter : Int { return 2 }
    
}

extension DetailViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//Generate WalkingMode,MixedTransit Cells
extension DetailViewController{
    
    func walkingModeOnlyCell(_ tableView:UITableView,forIndexPath indexPath:IndexPath) -> UITableViewCell{
        if indexPath.row == 0{
            let cell: WalkingOnlyODTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.imageViewWalking.image = #imageLiteral(resourceName: "marker_dark")
            let originInfo = detailPresenter.getTableOriginHeader()
            cell.labelLocationTitile.text = originInfo.title
            cell.labelLocationDetail.text = originInfo.desc
            return cell
        }else if indexPath.row == self.items?.count.advanced(by: numberOfHeaderFooter).advanced(by: -1){
            let cell: WalkingOnlyODTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.imageViewWalking.image = #imageLiteral(resourceName: "marker-red")
            let destinationInfo = detailPresenter.getTableDestinationFooter()
            cell.labelLocationTitile.text = destinationInfo.title
            cell.labelLocationDetail.text = destinationInfo.desc
            return cell
        }else{
            let cell: WalkingOnlyStepTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            let item = self.items![indexPath.row - 1]
            cell.imageViewStepSign.image = UIImage(named: item.maneuver ?? "dontloadimage")
            cell.labelDirectionInstruction.text = item.html_instructions?.toAttributedHTMLString?.string
            cell.labelDistanceValue.text = item.distance?.text
            return cell
        }
        
    }
    
    
    func mixedTransitModeCell(_ tableView:UITableView,forIndexPath indexPath:IndexPath) -> UITableViewCell{
        if indexPath.row == 0{
            let cell: TransitMixedODTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.imageViewTransit.image = #imageLiteral(resourceName: "marker_dark")
            let originInfo = detailPresenter.getTableOriginHeader()
            cell.labelTitle.text = originInfo.title
            cell.labelTime.text = originInfo.desc
            return cell
        }else if indexPath.row == self.items?.count.advanced(by: numberOfHeaderFooter).advanced(by: -1){
            let cell: TransitMixedODTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.imageViewTransit.image = #imageLiteral(resourceName: "marker-red")
            let destinationInfo = detailPresenter.getTableDestinationFooter()
            cell.labelTitle.text = destinationInfo.title
            cell.labelTime.text = destinationInfo.desc
            return cell
        }else{
            
            let item = self.items![indexPath.row - 1]
            if case .walking? = item.travel_mode{
                let cell: TransitMixedWalkingTableViewCell = tableView.dequeueReusableCell(for: indexPath)
                cell.labelTitleWalking.text = "Walk \(item.duration?.text ?? "0s") (\(item.distance?.text ?? "0m"))"
                return cell
            }else{
                let cell: TransitMixedTransitTableViewCell = tableView.dequeueReusableCell(for: indexPath)
                cell.labelFromLocation.text = item.transit_details?.departure_stop?.name
                cell.labelToLocation.text = item.transit_details?.arrival_stop?.name
                
                //Vehicle icon
                let transitIconUrlStr = item.transit_details?.line?.vehicle?.local_icon ?? item.transit_details?.line?.vehicle?.icon
                if let transitIconStr = transitIconUrlStr,let transitIconUrl = URL(string: "https:" + transitIconStr){
                    cell.imageViewTransit.kf.setImage(with: transitIconUrl)
                }
                
                //VehicleName_HeadSign
                if let transitSName = item.transit_details?.line?.short_name{
                    let attributedTransitSName = NSMutableAttributedString(string: transitSName)
                    if let textColorHex = item.transit_details?.line?.text_color{
                        attributedTransitSName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hex: textColorHex), range: NSRange(location: 0, length: transitSName.count))
                    }
                    if let backGroundColorHex = item.transit_details?.line?.color{
                        attributedTransitSName.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor(hex: backGroundColorHex), range: NSRange(location: 0, length: transitSName.count))
                        
                        cell.stripViewForColoring.backgroundColor = UIColor(hex: backGroundColorHex)
                    }
                    
                    let headSignAttStr = NSAttributedString(string: " \(item.transit_details?.headsign ?? "")")
                    attributedTransitSName.append(headSignAttStr)
                    cell.labelVehicleName_HeadSign.attributedText = attributedTransitSName
                }
                
                cell.labelVehicleDepartureTime.text = item.transit_details?.departure_time?.text
                cell.labelVehicleDescription.text = ""//not given
                if let numberOfStops = item.transit_details?.num_stops,numberOfStops != 0{
                    cell.labelTotalStop.text = "⇟ Ride \(numberOfStops) stops"
                }
                return cell
            }
            
            
//            cell.imageViewStepSign.image = UIImage(named: item.maneuver ?? "dontloadimage")
//            cell.labelDirectionInstruction.text = item.html_instructions?.toAttributedHTMLString?.string
//            cell.labelDistanceValue.text = item.distance?.text
        }
        
    }
    
    
    
}



//Uncomment for collapsible implementation
/*
 
 
 //
 // MARK: - Section Header Delegate
 //
 extension DetailViewController: CollapsibleTableViewHeaderDelegate {
 
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int) {
        let hasCollapsed = !sections[section].isCollapsed
 
        // Toggle collapse
        sections[section].isCollapsed = hasCollapsed
        header.setCollapsed(hasCollapsed)
        tableViewDetail.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }  
 
 }
 
extension DetailViewController:UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].isCollapsed ? 0 : sections[section].items.count
    }
    
    // Cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CollapsibleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CollapsibleTableViewCell ??
            CollapsibleTableViewCell(style: .default, reuseIdentifier: "cell")
        
        let item = sections[indexPath.section].items[indexPath.row]
        
        cell.lableTitle.attributedText = item.title.toAttributedHTMLString
        cell.labelDetail.text = item.description
        
        return cell
    }
    
    
}

extension DetailViewController : UITableViewDelegate{
    // Header
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
        
        header.labelTitle.text = sections[section].title
        header.labelArrow.text = ">"
        header.setCollapsed(sections[section].isCollapsed)
        header.section = section
        header.delegate = self
        
        return header
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
}

 
 */
