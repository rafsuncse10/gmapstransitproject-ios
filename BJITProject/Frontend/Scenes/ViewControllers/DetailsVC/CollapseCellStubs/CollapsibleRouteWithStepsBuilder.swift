//
//  CollapsibleRouteWithStepsBuilder.swift
//  BJITProject
//
//  Created by RAFAT TOUQIR RAFSUN on 3/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation

struct RouteSection : CollapsibleSection {
    typealias Item = Steps
    
    var title: String{
        
        return self.step.html_instructions ?? ""
        
    }
    
    var description: String?
    
    var items: [Steps]{
        return self.step.steps ?? []
    }
    
    var isCollapsed: Bool = false
    
    
    let step:Steps
//    let isWalkingModeOnly:Bool
    
    init(step:Steps) {
        self.step = step
//        //Walking mode only generation
//        self.isWalkingModeOnly = route.legs?.first?.arrival_time != nil
        
        
    }
    
}

extension Steps : CollapsibleItem{
    var title: String {
        return self.html_instructions ?? ""
    }
    
    var description: String? {
        return self.distance?.text
    }
    
    
}


