//
//  CollapsibleTableViewCell.swift
//  ios-swift-collapsible-table-section
//
//  Created by Yong Su on 7/17/17.
//  Copyright © 2017 Yong Su. All rights reserved.
//

import UIKit

class CollapsibleTableViewCell: UITableViewCell {
    
    let lableTitle = UILabel()
    let labelDetail = UILabel()
    
    // MARK: Initalizers
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let marginGuide = contentView.layoutMarginsGuide
        
        // configure nameLabel
        contentView.addSubview(lableTitle)
        lableTitle.translatesAutoresizingMaskIntoConstraints = false
        lableTitle.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lableTitle.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        lableTitle.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        lableTitle.numberOfLines = 0
        lableTitle.font = UIFont.systemFont(ofSize: 16)
        
        // configure detailLabel
        contentView.addSubview(labelDetail)
        labelDetail.lineBreakMode = .byWordWrapping
        labelDetail.translatesAutoresizingMaskIntoConstraints = false
        labelDetail.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        labelDetail.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        labelDetail.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        labelDetail.topAnchor.constraint(equalTo: lableTitle.bottomAnchor, constant: 5).isActive = true
        labelDetail.numberOfLines = 0
        labelDetail.font = UIFont.systemFont(ofSize: 12)
        labelDetail.textColor = UIColor.lightGray
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
