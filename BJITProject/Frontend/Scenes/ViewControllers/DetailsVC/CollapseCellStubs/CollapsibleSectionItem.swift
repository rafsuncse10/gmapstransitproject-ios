//
//  CollapsibleSectionItem.swift
//  BJITProject
//
//  Created by RAFAT TOUQIR RAFSUN on 3/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import Foundation


protocol CollapsibleSection {
    associatedtype Item : CollapsibleItem
    var title: String { get }
    var description:String? { get }
    var items: [Item] { get }
    var isCollapsed: Bool { get set }
}



protocol CollapsibleItem {
    var title: String { get }
    var description: String? { get }
}
