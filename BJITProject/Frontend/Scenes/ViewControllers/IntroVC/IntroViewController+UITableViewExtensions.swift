//
//  IntroViewController+UITableViewExtensions.swift
//  BJITProject
//
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import UIKit


extension IntroViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return introPresenter.routes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RouteTableViewCell
        
        let route = introPresenter.routes[indexPath.row]
        let routeStepMSG = NSMutableAttributedString(string: "")
        
        route.legs?.first?.steps?.enumerated().forEach{ index,step in
            
            if case .transit? = step.travel_mode{
                
                let transitIconUrlStr = step.transit_details?.line?.vehicle?.local_icon ?? step.transit_details?.line?.vehicle?.icon
                if let transitIconStr = transitIconUrlStr,let transitIconUrl = URL(string: "https:" + transitIconStr){
                    //print(transitIconUrl.absoluteString)
                    let transitIconAttachment = AsyncTextAttachment(imageURL: transitIconUrl, onCompletion: { (imageAttachment, isSizeChanged) in
                        cell.labelSteps.setNeedsDisplay()
                    })
                    transitIconAttachment.displayBounds = CGRect(x: 0, y: -4, width: 20, height: 20)
                    let transitIconAttachmentStr = NSAttributedString(attachment: transitIconAttachment)
                    
                    routeStepMSG.append(transitIconAttachmentStr)
                }
                
                if let transitSName = step.transit_details?.line?.short_name{
                    let attributedTransitSName = NSMutableAttributedString(string: transitSName)
                    if let textColorHex = step.transit_details?.line?.text_color{
                        attributedTransitSName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hex: textColorHex), range: NSRange(location: 0, length: transitSName.count))
                    }
                    if let backGroundColorHex = step.transit_details?.line?.color{
                        attributedTransitSName.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor(hex: backGroundColorHex), range: NSRange(location: 0, length: transitSName.count))
                    }
                    
                    routeStepMSG.append(attributedTransitSName)
                }
                
            }else if case .walking? = step.travel_mode{
                
                
                let walkingImageAttachment = AsyncTextAttachment()
                walkingImageAttachment.image = #imageLiteral(resourceName: "Walking")
                walkingImageAttachment.displayBounds = CGRect(x: 0, y: -4, width: 20, height: 20)
                let walkingImageAttachmentStr = NSAttributedString(attachment: walkingImageAttachment)
                
                routeStepMSG.append(walkingImageAttachmentStr)
                
                if let stepDuration = step.duration?.value?.toDouble.toStringInTime(style: .abbreviated){
                    
                    let stepDurationAttr = NSAttributedString(string: stepDuration, attributes: [
                        NSAttributedString.Key.font : UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
                        NSAttributedString.Key.baselineOffset : -5
                        ])
                    
                    routeStepMSG.append(stepDurationAttr)
                    
                }
                
            }else{
                let nonTransitTypeStr = step.travel_mode?.rawValue.first?.toString ?? ""
                
                routeStepMSG.append(NSAttributedString(string: nonTransitTypeStr))
            }
            
            let totalSteps = route.legs!.first!.steps!.count
            if totalSteps > 1 && index != (totalSteps - 1){
                routeStepMSG.append(NSAttributedString(string: " > "))
            }
        }
        let routeDuration = route.legs?.first?.duration?.value?.toDouble.toStringInTime(style: .short)
        cell.labelSteps.attributedText = routeStepMSG
        cell.labelDuration.text = routeDuration
        
        if let arrivalTimeVal = route.legs?.first?.arrival_time?.value?.toDouble,
            let departureTimeVal = route.legs?.first?.departure_time?.value?.toDouble {
            let arrivalDate = Date(timeIntervalSince1970: arrivalTimeVal)
            let departureDate = Date(timeIntervalSince1970: departureTimeVal)
            
            
            cell.labelStartToEndTime.text = "\(DateHelper.timeInHourWeekDay(date: departureDate)) - \(DateHelper.timeInHourWeekDay(date: arrivalDate))"
        }else{
            cell.labelStartToEndTime.text = ""
        }
        
        let endPoint = route.legs?.first?.steps?.last{$0.transit_details != nil}
        if let endPointName = endPoint?.transit_details?.arrival_stop?.name,
            let endPointStationDepartureTimeValue = endPoint?.transit_details?.departure_time?.value?.toDouble{
            let endPointDepartureDate = Date(timeIntervalSince1970: endPointStationDepartureTimeValue)
            cell.labelDetails.text = "\(endPointDepartureDate.timeWithPeriod) from \(endPointName)"
        }else{
            var summary = route.summary ?? ""
            if summary.isEmpty{
                summary = "unnamed roads"
            }
            cell.labelDetails.text = "via \(summary)"
        }
        
        
        
        return cell
        
    }
    
    
}



extension IntroViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let route = introPresenter.routes[indexPath.row]
        self.showNextScreen(for: route)
    }
}
