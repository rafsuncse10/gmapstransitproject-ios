//
//  IntroViewController.swift
//  BJITProject
//
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import UIKit
import GooglePlaces

enum SelectedLocation {
    case none
    case startLocation
    case destinationLocation
}


class IntroViewController: UIViewController {

    @IBOutlet weak var buttonOrigin: UIButton!
    @IBOutlet weak var buttonDestination: UIButton!
    
    
    @IBOutlet weak var tableViewRoutes: UITableView!
    
    
    
    let introPresenter = IntroPresenter()
    var selectedLocation:SelectedLocation = .none
    var locationTuple:(origin:CLLocationCoordinate2D?,destination:CLLocationCoordinate2D?) = (nil,nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableViewRoutes.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        introPresenter.attach(this: self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        introPresenter.detach(this: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        let origin = "Biberstraße, 41564 Kaarst, Germany"
//        let destination = "Norf, 41469 Neuss, Germany"
////        let origin = "Ohanajaya station"
////        let destination = "Ayase Station"
//        startRoutesFetch(from: origin, to: destination)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    
    //MARK:- ACTIONS
    
    @IBAction func originButtonTapped(_ sender: UIButton) {
        openGMapsLocationSelector(for: .startLocation)
    }
    
    @IBAction func destinationButtonTapped(_ sender: UIButton) {
        openGMapsLocationSelector(for: .destinationLocation)
    }
    
    func openGMapsLocationSelector(for selectedLoc:SelectedLocation){
        let autoCompleteController = GMSAutocompleteViewController()
        autoCompleteController.delegate = self
        
        // selected location
        self.selectedLocation = selectedLoc
        self.present(autoCompleteController, animated: true, completion: nil)
    }
    
    func startRoutesFetch(from origin:CLLocationCoordinate2D, to destination:CLLocationCoordinate2D){
        let origin = "\(origin.latitude),\(origin.longitude)"
        let destination = "\(destination.latitude),\(destination.longitude)"
        startRoutesFetch(from: origin, to: destination)
    }
    
    func startRoutesFetch(from origin:String, to destination:String){
//        let origin = "Biberstraße, 41564 Kaarst, Germany"
//        let destination = "Norf, 41469 Neuss, Germany"
//                let origin = "Ohanajaya station"
//                let destination = "Ayase Station"
        introPresenter.onLocationSubmitted(origin: origin, destination: destination)
    }
    

}

//MARK:- GMSAutocompleteViewControllerDelegate
extension IntroViewController:GMSAutocompleteViewControllerDelegate{
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        switch selectedLocation {
        case .startLocation:
            buttonOrigin.setTitle(place.name, for: .normal)
            locationTuple.origin = place.coordinate
        case .destinationLocation:
            buttonDestination.setTitle(place.name, for: .normal)
            locationTuple.destination = place.coordinate
        default:
            break
        }
        
        if let origin2d = locationTuple.origin,let destination2d = locationTuple.destination{
            startRoutesFetch(from: origin2d, to: destination2d)
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("GmapsError \(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}


extension IntroViewController:IntroView{
    func startLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func finishLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        tableViewRoutes.reloadData()
    }
    func updateRouteView() {
        tableViewRoutes.reloadData()
        
    }
    
    func showNextScreen(for route: Route) {
        
        let detailPresenter = DetailPresenter()
        detailPresenter.route = route
        
        let detailViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        detailViewController.detailPresenter = detailPresenter
        
        let navVC = UINavigationController(rootViewController: detailViewController)
        self.present(navVC, animated: true, completion: nil)
    }
    
    
}
