//
//  RouteTableViewCell.swift
//  BJITProject
//
//  Created by RAFAT TOUQIR RAFSUN on 03/11/18.
//  Copyright © 2018 RAFAT TOUQIR RAFSUN. All rights reserved.
//

import UIKit

class RouteTableViewCell: UITableViewCell {

    @IBOutlet weak var labelSteps: UILabel!
    
    @IBOutlet weak var labelDuration: UILabel!
    
    @IBOutlet weak var labelStartToEndTime: UILabel!
    
    @IBOutlet weak var labelDetails: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
